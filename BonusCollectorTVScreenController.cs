﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusCollectorTVScreenController : MonoBehaviour {

    BonusCollectorController bonusCollectorController;

    public Text status;
    public Text timeLeft;

	// Use this for initialization
	void Start ()
    {
        bonusCollectorController = GetComponent<BonusCollectorController>();	
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(bonusCollectorController.isShooting )
        {
            status.text = "COLLECTING...";
            status.color = Color.green;
            timeLeft.text = string.Format("{0:0}", bonusCollectorController.timeLeft);
        }
        else
        {
            status.text = "OFF";
            status.color = Color.red;
            timeLeft.text = "---";
        }


	}
}
