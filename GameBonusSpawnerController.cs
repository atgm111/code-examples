﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameBonusSpawnerController : MonoBehaviour {
    //класс контролирует спавн бонусов после смерти крипов или тайлов, а так же добавляет энергию за каждого убитого крипа
    //при смерти врага вызывается проверка шанса на дроп и выбор выпавшенго бонуса, после чего бонус спавниться в сцену
    //так же в классе хранится конфиг всех шансов и типо бонусов


    public bool ShowDebug = true;

    public bool isBonusesAbleToDrop = false;

    public float creepCharge = 35f;

    public List<BonusDropChanceConfiguration> configsForEnemies = new List<BonusDropChanceConfiguration>();

   
    //методы для вызова проверки на спавн бонуса
    //вызывать при смерти и передавать тип убитого врага
    public void RegisterKillOfThisEnemy(BOT.TileTypes type, Vector3 deadPos)
    {
        MainDataContainer.instance.player.AddCollectorCharge((int)creepCharge * 2);
        if ((FindConfigForThisEnemy(type) != null) && (isBonusesAbleToDrop))
        {
            GameBonusScript.BonusType resultType = FindConfigForThisEnemy(type).AttemptToDropBonus();
            if (resultType != GameBonusScript.BonusType.noType) SpawnBonus(resultType, deadPos);
            if (ShowDebug) Debug.LogFormat("GameBonusSpawnerController: Tile Killed, creep type {0}", type);
        }
    }

    public void RegisterKillOfThisEnemy(BOT.CreepTypes type, Vector3 deadPos)
    {
        MainDataContainer.instance.player.AddCollectorCharge((int)creepCharge);
        type = BOT.CreepTypes.Random;
        if ((FindConfigForThisEnemy(type) != null) && (isBonusesAbleToDrop))
        {
            GameBonusScript.BonusType resultType = FindConfigForThisEnemy(type).AttemptToDropBonus();
            if (resultType != GameBonusScript.BonusType.noType) SpawnBonus(resultType, deadPos);
            if (ShowDebug) Debug.LogFormat("GameBonusSpawnerController: Creep Killed, creep type {0}", type);
        }
    }


    //поиск конфига для данного врага
    BonusDropChanceConfiguration FindConfigForThisEnemy(BOT.CreepTypes type)
    {
        foreach(BonusDropChanceConfiguration enemyConfig in configsForEnemies)
        {
            if ((enemyConfig.creepType == type) && (enemyConfig.isCreepConfig))
                return enemyConfig;
        }
        Debug.Log("<color=red>GameBonusSpawner: Creep config not found!</color>");
        return null;
    }

    BonusDropChanceConfiguration FindConfigForThisEnemy(BOT.TileTypes type)
    {
        foreach (BonusDropChanceConfiguration enemyConfig in configsForEnemies)
        {
            if (enemyConfig.tileType == type)
                return enemyConfig;
        }
        Debug.Log("<color=orange>GameBonusSpawner: Tile config not found!</color>");
        return null;
    }

    public void SpawnBonus(GameBonusScript.BonusType type, Vector3 spawnPosition)
    {
        GameObject bonusToSpawn = ObjectPool.GetBonusByType(type);
        bonusToSpawn.SetActive(true);
        GameBonusScript gameBonusScriptToSpawn = bonusToSpawn.GetComponent<GameBonusScript>();
        gameBonusScriptToSpawn.Spawn(spawnPosition);
        if (ShowDebug) Debug.LogFormat("GameBonusSpawnerController: <color=green>Bonus is spawned succesefully</color>");
    }
    

    //тестовая функция проверки всех спавнов
    void TestChancesDropsForEnemy(int testCount, BOT.TileTypes type)
    {
        for (int i = 0; i < testCount; i++)
       {
            FindConfigForThisEnemy(type).AttemptToDropBonus(); 
       }
    }

    void Start ()
    {
        //TestChancesDropsForEnemy(1000, BOT.TileTypes.Bomb);
        
	}
	
	
	void Update ()
    {
		
	}
}

[Serializable]
public class BonusDropChanceConfiguration
{
    //класс конфигурации шансов и логики дропа бонусов 
    public bool isCreepConfig;
    public BOT.CreepTypes creepType;
    public BOT.TileTypes tileType;
    public int maximumTriesWithoutDrop = 5; //сколько неудачных попыток надо, чтобы выпал бонус (падает 100% после maximumTriesWithoutDrop неудачных попыток)
    int triesCounter;

    public bool showShortDebug;
    public bool showLongDebug;

    public float chanceForDrop = 0f;

    public List<DropChanceForBonus> bonusesDropChances = new List<DropChanceForBonus>();


    //попытка дропнуть бонус, возвращает удачно или неудачно и тип бонуса, НЕ спавнит
    public GameBonusScript.BonusType AttemptToDropBonus()
    {
        triesCounter++;
        var rnd = (int)UnityEngine.Random.Range(1, 100);
        if (triesCounter >= maximumTriesWithoutDrop)
        {
            if(showLongDebug || showShortDebug) Debug.LogFormat("GameBonusSpawner: Bonus Dropped becuase maximum attempts count = {0} is reached", triesCounter);
            triesCounter = 0;
            return GetRandomBonus();
        }
        else 
        if (rnd <= chanceForDrop)
        {
            if (showLongDebug) Debug.LogFormat("GameBonusSpawner: Rolled chance is {0}, bonus is dropped", rnd);
            if (showShortDebug) Debug.Log("Bonus Is Dropped");
            triesCounter = 0;
            return GetRandomBonus();
        }
        else
        {
            if (showLongDebug) Debug.LogFormat("GameBonusSpawner: Rolled chance is {0}, bonus isn't dropped", rnd);
            if (showShortDebug) Debug.Log("Bonus Is NOT Dropped");
            return GameBonusScript.BonusType.noType;
        }
        
       
    
    }

    //выбор рандомного бонуса при выпадении бонуса
    GameBonusScript.BonusType GetRandomBonus()
    {
        var rnd = (int)UnityEngine.Random.Range(1, 100);
        foreach (DropChanceForBonus bonusDrop in bonusesDropChances)
        {
            if(rnd <= bonusDrop.chance)
            {
                if (showLongDebug) Debug.LogFormat("Bonus of type {0} is dropped, rolled chance is {1}", bonusDrop.type, rnd);
                if (showShortDebug) Debug.LogFormat("Bonus of type {0} is dropped", bonusDrop.type);
                return bonusDrop.type;
            }
            else
            {
                if (showLongDebug) Debug.LogFormat("Move next, rolled is {0}, bonusDropChance is {1}", rnd, bonusDrop.chance);
                rnd -= bonusDrop.chance;
            }
        }
        Debug.Log("<color=red>GetRandomBonus: Critical logic Error, check for all chances</color>");
        return GameBonusScript.BonusType.noType;
        
    }
}

[Serializable]
public class DropChanceForBonus
{
    public GameBonusScript.BonusType type;
    public int chance;
}