﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusCollectorController : MonoBehaviour {
    //скрипт, контролирующий работу пылесоса-сборщика бонусов
    public bool isShooting;


    [Header("Включить для отладки")]
    public bool ShowDebug = false;

    [Space(30)]
    [Header("Опции положения и перемещения")]
    public GameObject energyFlowStartPoint;
    public GameObject InactivePosition;
    public GameObject ActivePosition;
    public float moveToActivePositionTime;
    public float moveToInactivePositionTime;
    public GameObject bonusCollectorMuzzleParticle;

    [Space(30)]
    [Header("Опции игровой логики")]
    public float CollectionDamagePerSecond = 100f;
    public float maximumAngle = 90f;
    public float autoOffAfterActiveTimeWithoutTarget = 2f;
    public float bonusMovingSpeed = 1f;
    public float bonusMinimumDistanceFromPlayer = 0.5f;

    [Space(30)]
    [Header("Данные монитора пылесоса")]
    public float timeLeft;
    public float collectingPercentProgress;
    public RectTransform crossSign;

    public GameBonusScript selectedTargetBonus;
    private bool isDealingDamageToBonus;
    private int resetedCollectorCharge = 0;
    private bool isTransitionIsOver;
    private bool isInActivePos;
    private float autoOffTimer;
    private bool isAutoOffStarted;

    #region public Methods
    public void StartShooting()
    {
        //начинаем стрельбу из пушки
        //сначала необходимо пушку достать
        TestRayShoot();
        StopAllCoroutines();
        StartCoroutine(LerpRotationAndPositionFromPointToPoint(transform, ActivePosition.transform, moveToActivePositionTime));
        Shoot();
        autoOffTimer = autoOffAfterActiveTimeWithoutTarget;
        isAutoOffStarted = true;
    }

    public void StopShooting()
    {
        //остановка стрельбы, сброс энергии, убирание пушки
        StopAllCoroutines();
        StartCoroutine(LerpRotationAndPositionFromPointToPoint(transform, InactivePosition.transform, moveToActivePositionTime));
        bonusCollectorMuzzleParticle.SetActive(false);
        bonusCollectorMuzzleParticle.SetActive(true);
        isDealingDamageToBonus = false;
        selectedTargetBonus = null;
        //ResetCharge();
    }

    public void ResetCharge()
    {
        //сбрасываем заряд на ноль после неудачи или после поглощения бонуса
        MainDataContainer.instance.player.collectorCharge = resetedCollectorCharge;
    }
    #endregion


    #region private Methods

    IEnumerator LerpRotationAndPositionFromPointToPoint(Transform startPoint, Transform endPoint, float time)
    {
        //перемещает пушку от одной точки до другой
        float timer = 0f;
        isTransitionIsOver = false;
        while(true)
        {
            transform.position = Vector3.Lerp(startPoint.position, endPoint.position, timer / time);
            transform.rotation = Quaternion.Lerp(startPoint.rotation, endPoint.rotation, timer / time);
            timer += Time.deltaTime;
            if(timer >= time)
            {
                transform.position = endPoint.position;
                transform.rotation = endPoint.rotation;
                isTransitionIsOver = true;
                break;
            }
            yield return null;
        }
    }


    void Shoot()
    {
        if ((selectedTargetBonus != null) && (selectedTargetBonus.isBonusAlive)) isDealingDamageToBonus = true; //если есть попадание в цель и цель активна то начинаем наносить урон
    }

    private void CheckAndDealDamage()
    {
        //в Update проверяем на 
        if(isDealingDamageToBonus && (selectedTargetBonus != null) && (selectedTargetBonus.gameObject.activeInHierarchy))
        {
            DealDamageToSelectedTargetBonus();
            collectingPercentProgress = selectedTargetBonus.currentBonusHealth / selectedTargetBonus.bonusHealth;
            MainDataContainer.instance.player.collectorCharge = (int)(collectingPercentProgress * MainDataContainer.instance.player.collectorMaxCharge);
        }

        if(isDealingDamageToBonus)
        if( (selectedTargetBonus == null) || (!selectedTargetBonus.gameObject.activeInHierarchy) || (!selectedTargetBonus.isBonusAlive))
        {
            StopShooting();
        }

        

    }

    private bool TestRayShoot()
    {
        //при первом выстреле проверяет попадание в цель
        //если попало в цель и цель является бонусом, выбирает его и начинает наносить урон
        int layerMask = 1 << 8;

        RaycastHit hit;
        Camera cam = Camera.main;

        if(Physics.Raycast(cam.gameObject.transform.position, cam.gameObject.transform.forward, out hit, Mathf.Infinity, layerMask))
        {
            //если попало в цель в данном layerMask и цель это бонус, то выбирает этот бонус как данную цель
            //Debug.DrawRay(cam.gameObject.transform.position, cam.gameObject.transform.forward, Color.blue, 5f);
            if(ShowDebug) Debug.DrawLine(cam.gameObject.transform.position, hit.transform.position, Color.blue, 5f);
            if (hit.transform.gameObject.GetComponentInParent<GameBonusScript>() != null) selectedTargetBonus = hit.transform.gameObject.GetComponentInParent<GameBonusScript>();
            return true;
        }
        else
        {
            selectedTargetBonus = null;
            return false;
        }
        
    }

    private void DealDamageToSelectedTargetBonus()
    {
        //наносит урон по цели каждый кадр
        selectedTargetBonus.DealDamage(CollectionDamagePerSecond * Time.deltaTime);

        //притягивает бонус
        //selectedTargetBonus.transform.LookAt(transform.position);
        Vector3 translation = transform.position - selectedTargetBonus.transform.position;
        selectedTargetBonus.transform.Translate(translation * bonusMovingSpeed, Space.World);// * Vector3.Distance(transform.position, selectedTargetBonus.transform.position), Space.World);
        /*if (Vector3.Distance(transform.position, selectedTargetBonus.transform.position) >= bonusMinimumDistanceFromPlayer)
        {
            
        }*/
    }

    private void UpdateTVScreenInfo()
    {
        //обновляет информацию, необходимую для отображения в экранчике пушки
        if (selectedTargetBonus != null)
        {
            timeLeft = selectedTargetBonus.currentBonusHealth / CollectionDamagePerSecond;
            collectingPercentProgress = selectedTargetBonus.currentBonusHealth / selectedTargetBonus.bonusHealth;
            crossSign.localScale = Vector3.one * 3.5f;
        }
        else
        {
            crossSign.localScale = Vector3.one * 2f;
        }
    }

    private void CheckForAllowedAngle()
    {
        //проверяет допустимый угол отклонения пушки от бонуса и сбрасывает, если угол превышен
        if (selectedTargetBonus != null)
        {

            Vector3 targetDir = selectedTargetBonus.transform.position - transform.position;
            float angle = Vector3.Angle(targetDir, transform.right);

            

            if (angle <= maximumAngle)
            {
                if (ShowDebug) Debug.DrawLine(energyFlowStartPoint.transform.position, selectedTargetBonus.gameObject.transform.position, Color.red);
                if (ShowDebug) Debug.LogFormat("Angle is {0}, maximum allowed angle is {1}, <color=green>IN zone</color>", angle, maximumAngle);
            }
            else
            {
                StopShooting();
                if (ShowDebug) Debug.LogFormat("Angle is {0}, maximum allowed angle is {1}, <color=red>OUT of zone</color>", angle, maximumAngle);
            }
        }
    }

    private void CheckForGunStage()
    {
        //проверяет и выставляет состояние пушки
        isShooting = isDealingDamageToBonus;
        

        if(transform.position == ActivePosition.transform.position)
        {
            isTransitionIsOver = true;
            isInActivePos = true;
        }
        else if(transform.position == InactivePosition.transform.position)
        {
            isTransitionIsOver = true;
            isInActivePos = false;
        }


        autoOffTimer -= Time.deltaTime;
        if((autoOffTimer <= 0f) && (isAutoOffStarted) && (!isDealingDamageToBonus))
        {
            isAutoOffStarted = false;
            StopShooting();
            //отнимаем энергию при промахе
            MainDataContainer.instance.player.AddCollectorCharge((int)-MainDataContainer.instance.gameBonusSpawnerController.creepCharge);
        }

    }

    

    // Use this for initialization
    void Start ()
    {
        //StopShooting();
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateTVScreenInfo();
        CheckForGunStage();
        CheckForAllowedAngle();
        CheckAndDealDamage();

        if (!isShooting)
        {
            TestRayShoot();
        }
	}
    #endregion

}
