﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameBonusScript : MonoBehaviour {

    public enum BonusType
    {
        noType,
        health,
        exp,
        score,
        ammo,
        shield,
        scoreMultiplier,
        rateOfFireMultiplier,
        healthByButton,
        timeSlow,
        bombDamage
    }

    [Header("Параметры объекта бонуса")]
    public float lifeTime;
    float lifeTimer;
    public float bonusHealth;
    public float currentBonusHealth;
    public bool isBonusAlive;
    public GameObject particle;
    public AudioClip bonusActivationSound;
    

    [Space(30)]
    [Header("Параметры бонуса")]
    public BonusType bonusType;
    public bool isInstantBonus = true;
    public int healthAdd = 0;
    public int ammoAdd = 0;
    public int scoreAdd = 0;
    public int expAdd = 0;
    public int shieldAdd = 0;
    public int damageAllEnemiesValue = 0;

    public float rateOfFireMultiplier = 1f;
    public float rateOfFireBonusLifeTime = 5f;

    public float scoreMultiplier = 1f;
    public float scoreBonusLifeTime = 5f;

    public float timeScaleEffect = 1f;
    public float timeScaleEffectTime = 5f;

    [Space(30)]
    [Header("Параметры аниматора")]
    public Animator animController;
    public float animationTimeBeforeOff = 2f;
    public bool isActive;
    public float audioVolume = 3f;

    public delegate void BonusCollect();
    public event BonusCollect OnBonusCollected;

    private float invinsibleTime = 0.3f;
    private float invinsibleTimer = 0f;
    private AudioSource audioSource;

    public delegate void OnBonusKilled();
    public static event OnBonusKilled BonusKilledEvent;



    public void AddBonusToPlayer()
    {
        bool isInstantEffectPlay = true;
            var playerDataWorker = MainDataContainer.instance.playerDataWorker;
            var player = MainDataContainer.instance.player;

            
            player.Heal(healthAdd);
        if (healthAdd > 0)
        {
            SpawnHPMessage(healthAdd);
        }
        //player.shield += shieldAdd;
        MainDataContainer.instance.scoreCollector.expirence += expAdd;
            MainDataContainer.instance.scoreCollector.score += scoreAdd;
            if (scoreAdd > 0)
            {
                SpawnScoreMessage(scoreAdd);
            }
            MainDataContainer.instance.scoreCollector.scoreMultiplier *= scoreMultiplier;
            var gunID = GameObject.FindObjectOfType<OnPointerClick>().CurrentGun.ID;
            //playerDataWorker.Data.GunAmmo[gunID] += (int)(playerDataWorker.Data.MaxAmmo[gunID] * ammoAddPercent);
            if (gunID == 0)
            {
                //если в руках рогатка то даем коины
                playerDataWorker.Data.AddCoins(20);
            }
            else
            {
                //или же даем патроны на выбранную пушку
                playerDataWorker.Data.GunAmmo[gunID] += ammoAdd;
            }


        if (bonusType == BonusType.rateOfFireMultiplier)
        {
            MainDataContainer.instance.bonusEffectController.AddRateOfFire(rateOfFireMultiplier, rateOfFireBonusLifeTime);
            MainDataContainer.instance.player.ShowMessage("x2 Attack Speed",Color.red);

        }
        if (bonusType == BonusType.timeSlow)
        {
            MainDataContainer.instance.bonusEffectController.SetTimeScaleAndResetAfterTime(timeScaleEffect, timeScaleEffectTime);
            MainDataContainer.instance.player.ShowMessage("Time Slow Down",Color.magenta);

        }
        if (bonusType == BonusType.bombDamage)
        {
            Invoke("DamageAllEnemies", 0.75f);
        }
        if (bonusType == BonusType.shield) MainDataContainer.instance.bonusEffectController.AddShield();

        if (isInstantEffectPlay)
        {
            if((audioSource != null) && (bonusActivationSound != null))
            {
                audioSource.PlayOneShot(bonusActivationSound);
            }
            MainDataContainer.instance.bonusEffectController.PlaySpecialBonusActionEffect(bonusType);
        }

        MainDataContainer.instance.bonusCollectorController.ResetCharge();

    }

    void DamageAllEnemies()
    {
        MainDataContainer.instance.bonusEffectController.DamageAllEnemies(damageAllEnemiesValue);
    }

    public void SpawnScoreMessage(int reward)
    {
        GameObject message = ObjectPool.GetMessageByType(InfoText.TargetType.Score);
        if (message != null)
        {
            message.GetComponent<InfoText>().Spawn(transform.position, reward);

        }
    }
    public void SpawnHPMessage(int reward)
    {
        GameObject message = ObjectPool.GetMessageByType(InfoText.TargetType.HP);
        if (message != null)
        {
            message.GetComponent<InfoText>().Spawn(reward);

        }
       // MainDataContainer.instance.player.ShowMessage("+ "+reward+" Health",Color.green);
    }

    public void DealDamage(float damage)
    {
        invinsibleTimer = invinsibleTime;
        currentBonusHealth -= damage;
        if (currentBonusHealth <= 0f)
            if (BonusKilledEvent != null)
            {
                BonusKilledEvent();
            }
        if ((currentBonusHealth <= 0f) && (isInstantBonus)) KillWithReward();
        if ((currentBonusHealth <= 0f) && (!isInstantBonus)) Capture();


        
    }



    public void Spawn(Vector3 position)
    {
        transform.position = position;
        isBonusAlive = true;
        lifeTimer = lifeTime;
        currentBonusHealth = bonusHealth;
    }

    public void KillWithReward()
    {
        MainDataContainer.instance.bonusEffectController.PlaySpecialBonusActionEffect(bonusType);
        AddBonusToPlayer();
        isBonusAlive = false;
        PlayDisableAnimation();
        Invoke("Kill", animationTimeBeforeOff);
        
    }


    

    void KillCapture()
    {
        transform.position = new Vector3(1000f, 0f, 0f);
    }


    public void Kill()
    {
        isBonusAlive = false;
        lifeTimer = lifeTime;
        currentBonusHealth = bonusHealth;
        animController.Rebind();
        ObjectPool.BackToPool(this);
        gameObject.SetActive(false);
    }

    public void Capture()
    {
        MainDataContainer.instance.bonusEffectController.SetCurrentBonus(this);
        PlayActivateAnimation();
        isBonusAlive = false;
        Invoke("PlayDisableAnimation", animationTimeBeforeOff);
        Invoke("KillCapture", animationTimeBeforeOff);
    }

    void PlayActivateAnimation()
    {
        animController.SetTrigger("Activate");
        audioSource.Play();
        MainDataContainer.instance.bonusEffectController.PlayKillOrCaptureEffect(bonusType);
    }



    void PlayDisableAnimation()
    {
        animController.SetTrigger("Disable");
    }

    private void FixedUpdate()
    {
        if (isBonusAlive)
        {
            invinsibleTimer -= Time.fixedDeltaTime;
            lifeTimer -= Time.fixedDeltaTime;
            if (invinsibleTimer <= 0f) currentBonusHealth = bonusHealth;
            if ((lifeTimer <= 0f) && (invinsibleTimer <= 0f)) Kill();
        }
    }

    private void Update()
    {
        if(invinsibleTimer >= 0f)
        {
            if (particle != null) particle.SetActive(true);
        }
        else
        {
            if (particle != null) particle.SetActive(false);
        }
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
}
